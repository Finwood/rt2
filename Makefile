TARGET ?= rt2

LATEXRUN       ?= latexrun

# Inkscape svg handling
INKSCAPE       ?= /usr/bin/inkscape
INKSCAPE_ARGS  ?= --export-latex --export-area-page
SVGDIR         ?= svg
SVGFILES       ?= $(basename $(notdir $(wildcard $(SVGDIR)/*.svg)))
SVGDEPS         = $(addsuffix .pdf,$(addprefix img-,$(SVGFILES))) \
                  $(addsuffix .pdf_tex,$(addprefix img-,$(SVGFILES)))

# tex file templates
SH_TEX_TEMPLATES       ?= $(shell find -type f -name '*.tex.sh')
RENDERED_SH_TEMPLATES   = $(SH_TEX_TEMPLATES:%.tex.sh=%.tex)
PY_TEX_TEMPLATES       ?= $(shell find -type f -name '*.tex.py')
RENDERED_PY_TEMPLATES   = $(PY_TEX_TEMPLATES:%.tex.py=%.tex)

TEMPLATES               = $(SH_TEX_TEMPLATES) $(PY_TEX_TEMPLATES)
RENDERED_TEMPLATES      = $(RENDERED_SH_TEMPLATES) $(RENDERED_PY_TEMPLATES)

.PHONY: pdf svgs templates run-anyway
pdf: $(TARGET).pdf
svgs: $(SVGDEPS)
templates: $(RENDERED_TEMPLATES)

$(TARGET).pdf: run-anyway templates svgs
	$(LATEXRUN) $(TARGET).tex

.PHONY: clean
clean:
	$(LATEXRUN) --clean-all
	/usr/bin/rm -f $(SVGDEPS) $(RENDERED_TEMPLATES)

img-%.pdf img-%.pdf_tex: $(SVGDIR)/%.svg
	$(INKSCAPE) $(INKSCAPE_ARGS) --file=$< --export-pdf=$@

build.tex: run-anyway
%.tex: %.tex.sh
	bash $< > $@

%.tex: %.tex.py
	python $< > $@
