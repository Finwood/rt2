#!/usr/bin/bash

# DRAFT: int (0 or 1)
# HREF: int (0 or 1)
# VERSION: string, arbitrary
# DATEFORMAT, TIMEFORMAT: string, see date(1)

DRAFT=${DRAFT:-0}
cat << EOF
\\newcount\\draft\\draft=$DRAFT
\\newcommand{\\ifdraft}[1]{\\ifnum\\draft>0 {#1} \\fi}
EOF

COMMIT_SHA="${CI_COMMIT_SHA:-$(git rev-parse HEAD 2>/dev/null)}"
COMMIT_REF_NAME="${CI_COMMIT_REF_NAME:-$(git rev-parse --abbrev-ref HEAD 2>/dev/null)}"
COMMIT_TAG="${CI_COMMIT_TAG:-$(git describe --tags $(git rev-list --tags --max-count=1) 2>/dev/null)}"
DIRTY=$([[ $(git diff --shortstat 2>/dev/null | tail -n1) != "" ]] && echo "*")
COMMIT_SHA_SHORT="${COMMIT_SHA:0:7}$DIRTY"
cat << EOF
\\newcommand{\\commitSha}{$COMMIT_SHA}
\\newcommand{\\commitShaShort}{$COMMIT_SHA_SHORT}
\\newcommand{\\commitRefName}{$COMMIT_REF_NAME}
\\newcommand{\\commitTag}{$COMMIT_TAG}
EOF

DATE=$(date "+${DATEFORMAT:-%Y-%m-%d}")
TIME=$(date "+${TIMEFORMAT:-%H:%M}")
cat << EOF
\\newcommand{\\builddate}{$DATE}
\\newcommand{\\buildtime}{$TIME}
EOF


VERSION=${VERSION:-${COMMIT_TAG:-"$COMMIT_SHA_SHORT ($COMMIT_REF_NAME)"}}

# if project URL present, wrap $VERSION in a commit link
if [ ${HREF:-1} -ne 0 ] && [ -n $CI_PROJECT_URL ]
then
  COMMIT_URL="$CI_PROJECT_URL/tree/${COMMIT_TAG:-$COMMIT_SHA}"
  VERSION="\\href{$COMMIT_URL}{$VERSION}"
fi

echo "\\newcommand{\\version}{$VERSION}"
